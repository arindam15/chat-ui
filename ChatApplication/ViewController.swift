//
//  ViewController.swift
//  ChatApplication
//
//  Created by Arindam Santra on 24/02/19.
//  Copyright © 2019 Arindam Santra. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var cvMessage: UICollectionView!
    
    let textMessage = [" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially un", "It is a long established fact that a reader will be distracted by the readable content of", " it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).", "arindam"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvMessage.dataSource = self
        cvMessage.delegate = self
        cvMessage.register(MessageCell.self, forCellWithReuseIdentifier: "MessageCell")
    }

    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textMessage.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let message = textMessage[indexPath.row]
        let size = CGSize(width: view.frame.width, height: 10000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimtedFrame = NSString(string: message).boundingRect(with: size, options: option, attributes: nil, context: nil)
        
        
        return CGSize(width: view.frame.width, height: estimtedFrame.height+20)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageCell", for: indexPath) as! MessageCell
        cell.textView.text = textMessage[indexPath.row]
        cell.textView.backgroundColor = .clear
        
        let message = textMessage[indexPath.row]
        let size = CGSize(width: view.frame.width, height: 10000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimtedFrame = NSString(string: message).boundingRect(with: size, options: option, attributes: nil, context: nil)
        cell.textView.frame = CGRectMake(8, 0, 250 + 24, estimtedFrame.height + 20)
        cell.textBubleView.frame = CGRectMake(8, 0, 250 + 24, estimtedFrame.height + 20)
        
        return cell
    }
    
    
    

}

