//
//  MessageCell.swift
//  ChatApplication
//
//  Created by Arindam Santra on 24/02/19.
//  Copyright © 2019 Arindam Santra. All rights reserved.
//

import UIKit

class MessageCell: UICollectionViewCell {
    
    let textView: UITextView = {
        let text  = UITextView()
        text.isScrollEnabled = false
        return text
    }()
    
    
    let textBubleView: UIView = {
        let bubleView = UIView()
        bubleView.layer.cornerRadius = 15
        bubleView.layer.masksToBounds = true
        bubleView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        return bubleView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(textBubleView)
        textBubleView.addSubview(textView)


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    
}
